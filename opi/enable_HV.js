importPackage(Packages.org.csstudio.opibuilder.scriptUtil); 

//------ CONSTANT ------------
var VERTICAL = "VERTICAL";
var HORIZONTAL = "HORIZONTAL";
//---- END  CONSTANT ---------

//----- FUNCTION -------------
function enable_widget(enable){
	widget.setPropertyValue("enabled",enable);
}
//--- END FUNCTION ------------

//----- MAIN ------------------
var VH_SELECTED = PVUtil.getDouble(pvs[0]); //HV_selected: 0:vertical, V:horizontal

var name = widget.getPropertyValue("name"); 
var widget_orientation;

//widget vertical or horizontal ?
if(name.indexOf(VERTICAL) > -1){
	widget_orientation = 0; //the widget is vertical
}
else if(name.indexOf(HORIZONTAL) > -1){
	widget_orientation = 1; //the widget is horizontal
}

if(VH_SELECTED==0){ //vertical selected
	if(widget_orientation == 0) //widget vertical
		enable_widget(true);
	else //widget horizontal
		enable_widget(false);
}
else { //horizontal selected
	if(widget_orientation == 0) //widget vertical
		enable_widget(false);
	else //widget horizontal
		enable_widget(true);
}

//---- END MAIN ---------------
