include $(EPICS_ENV_PATH)/module.Makefile

#STARTUPS = startup/emittanceTest.cmd
#Startup script removed from module, IOC startup is handled by parent module "emu".
DOC = doc/README.md
OPIS += opi/testEmitance_1100x600.opi
OPIS += opi/enable_HV.js
OPIS += opi/cea.png
OPIS += opi/ess2.png
OPIS += opi/irfu.png
OPIS += opi/vnadot.png
#USR_DEPENDENCIES = <module>,<version>

MISCS += misc/emittance.CFG
MISCS +=protocol/pmacVariables.proto
